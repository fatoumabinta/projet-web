<?php


namespace model;


class CommentModel
{
    //methode pour afficher la liste de commentaire d'un produit
    static function listComment(int $id_product):array{
        $db=\model\Model::Connect();
        $sql="SELECT content,date,firstname,lastname FROM comment INNER JOIN account ON comment.id_account=account.id WHERE id_product=?";
        $req=$db->prepare($sql);
        $req->execute(array($id_product));
        return $req->fetchAll();
    }
    //methode pour inserer un commentaire
    static function insertComment(int $id_product,string $content,string $mail):bool{
        $db=\model\Model::Connect();
        $sql="INSERT INTO comment (content,date,id_product,id_account) VALUES(?,?,?,?)";
        $req=$db->prepare($sql);
        $id_account=self::searchIdUser($mail);
        if($id_account==-1)return false;
        echo $id_account;
        if($req->execute(array($content,date("Y-m-d"),$id_product,$id_account))) return true;
        return false;
    }

    static function searchIdUser($mail):int{
        $db=\model\Model::connect();
        $sql="SELECT id FROM account WHERE mail=?";
        $req=$db->prepare($sql);
        $req->execute(array($mail));
        if($idUser=$req->fetch()) return $idUser['id'];
        return -1;
    }
}