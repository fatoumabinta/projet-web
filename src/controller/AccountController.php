<?php


namespace controller;


class AccountController
{
    public function account(){
        $params=array(
            "title"=>"Account",
            "module"=>"account.php"
        );
        \view\Template::render($params);
    }

    //methode qui fait appel appel au model pour inscrire un utilisateur
    public function signin(){
        $fn=htmlspecialchars($_POST['userfirstname']);
        $ln=htmlspecialchars($_POST['userlastname']);
        $ml=htmlspecialchars($_POST['usermail']);
        $ps=htmlspecialchars($_POST['userpass']);
        $_POST=array();
        if(\model\AccountModel::signin($fn,$ln,$ml,$ps)){
            header('Location:/account?status=signin_succes');
            exit();
        }else{
            header('Location:/account?status=signin_fail');
            exit();
        }
    }

    //methode qui fait appel au model pour selectionner un utilisateur pour se connecter
    public function login(){
        $ml=htmlspecialchars($_POST['usermail']);
        $ps=htmlspecialchars($_POST['userpass']);
        $data=\model\AccountModel::login($ml,$ps);
        if($data!=null)
        {
            $_SESSION['userfirstname']=$data['firstname'];
            $_SESSION['userlastname']=$data['lastname'];
            $_SESSION['usermail']=$data['mail'];
            $_SESSION['cart']=array();
            header('Location:/store');
            exit();
        }else{
            header('Location:/account?status=login_fail');
            exit();
        }
    }

    //methode qui permet de se deconnecter et aller vers account
    public function logout(){
        session_destroy();
        header('Location:/account?status=logout_succes');
        exit();
    }

    //methode qui affiche une page permettant de modifier ces informations
    public function infos(){

        if(isset($_SESSION['usermail'],$_SESSION['userfirstname'],$_SESSION['userlastname'])){
            $params=array(
              "title"=>"Informations du Compte",
                "module"=>"infos.php"
            );
            \view\Template::render($params);
        }else{
            header('Location: /account');
            exit();
        }
    }

    //methode qui fait la mise a jour des informatios modifie de l'utilisateur
    public function update(){
        if(isset($_SESSION['usermail'],$_SESSION['userfirstname'],$_SESSION['userlastname'])){
            $tabInfoUpdate=array(
                "firstnameUpdate"=>htmlspecialchars($_POST['firstnameUpdate']),
                "lastnameUpdate"=>htmlspecialchars($_POST['lastnameUpdate']),
                "mailUpdate"=>htmlspecialchars($_POST['mailUpdate']),
                "mail"=>$_SESSION['usermail']
            );
            reset($_POST);
            if(\model\AccountModel::updateInfos($tabInfoUpdate)){
                $_SESSION['userfirstname']=$tabInfoUpdate['firstnameUpdate'];
                $_SESSION['userlastname']=$tabInfoUpdate['lastnameUpdate'];
                $_SESSION['usermail']=$tabInfoUpdate['mailUpdate'];
                header('Location: /account/infos?statusUpdate=succes');
                exit();
            }
            header('Location: /account/infos?statusUpdate=fail');
            exit();
        }
        header('Location: /account');
        exit();
    }

    //methode pour supprimer le compte de l'utilisateur
    public function deleteAccount(){
        if(isset($_SESSION['usermail'])){
            if(\model\AccountModel::deleteUser($_SESSION['usermail'])){
                session_destroy();
                header('Location: /account?deleteUser=succes');
                exit();
            }

        }
        header('Location: /account');
        exit();

    }
}