<?php

namespace controller;

class StoreController
{

    public function store(): void
    {
        // Communications avec la base de données
        $categories = \model\StoreModel::listCategories();
        $produits = \model\StoreModel::listProducts();

        // Variables à transmettre à la vue
        $params = array(
            "title" => "Store",
            "module" => "store.php",
            "categories" => $categories,
            "produits" => $produits
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    //methode qui affiche les info d'un produit
    public function product(int $id)
    {

        //communication avec la base donée
        $produit = \model\StoreModel::infoProduct($id);

        if ($produit == null) {
            header('Location: /store');
            exit();
        }
        //chargement des avis du produit
        $comment = \model\CommentModel::listComment($id);
        //variavble a transmettre a la vue
        $params = array(
            "title" => "product",
            "module" => "product.php",
            "produit" => $produit,
            "comment" => $comment
        );

        //Faire le rendu de la vue
        \view\Template::render($params);
    }

    //methode qui affiche les produit en fonction des criteres de recherches
    public function search()
    {
        $dataSearch = array();
        $dataSearch['search'] =(!empty($_POST['search'])) ? htmlspecialchars($_POST['search']) : null;
        $dataSearch['category'] =(!empty($_POST['category'])) ? $_POST['category'] : null;
        $dataSearch['order'] =(!empty($_POST['order'])) ? htmlspecialchars($_POST['order']) : null;

        // Communications avec la base de données
        $categories = \model\StoreModel::listCategories();
        $produits = \model\StoreModel::searchListProducts($dataSearch);
        $searchProduct=(empty($produits)) ? "ProductNotFound" :  "ProductFound";
        // Variables à transmettre à la vue
        $params = array(
            "title" => "Store",
            "module" => "store.php",
            "categories" => $categories,
            "produits" => $produits,
            "searchProduct"=>$searchProduct
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }
}