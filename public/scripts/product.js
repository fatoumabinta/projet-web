
document.addEventListener('DOMContentLoaded',function(){
    let btnPlus=document.getElementById('plus')
    let btnMoins=document.getElementById('moins')
    let valQuantite=document.getElementById('quantite')
    let nbQuantite=parseInt(valQuantite.textContent)
    let imgPrincipale=document.querySelector('.product-images img')
    let imgMiniatures=document.querySelectorAll('.product-miniatures div>img')
    let formPostComment=document.getElementById('postComment')
    const QuantiteMax=5
    let inputHidden=document.getElementById('quantiteHidden')
    inputHidden.value=nbQuantite

    //Evenement pour augmenter la quantite du produit
    btnPlus.addEventListener('click',function(){
        if(nbQuantite<QuantiteMax){
            nbQuantite+=1
            inputHidden.value=nbQuantite
            valQuantite.textContent=nbQuantite
            if(nbQuantite===QuantiteMax) document.getElementById('msgQuantiteMax').style.visibility='visible'
        }
    })

    //Evenement pour diminuer la quantite du produit
    btnMoins.addEventListener('click',function(){
        if(nbQuantite>1){
            nbQuantite-=1
            inputHidden.value=nbQuantite
            valQuantite.textContent=nbQuantite
            if(nbQuantite===QuantiteMax-1)document.getElementById('msgQuantiteMax').style.visibility='hidden'
        }
    })
    //boulce pour faire un Evenement pour le click sur l'image miniature pour l'agrandir
    for(let elem of imgMiniatures){
        elem.addEventListener('click',function(){
            imgPrincipale.setAttribute('src',elem.getAttribute('src'));
        })
    }
    //Evenement pour envoyer le commentaire si c'est pas vide
    formPostComment.addEventListener('submit',function(event){
        event.preventDefault();
        let contentPost=document.getElementById('contentPost')
        if(contentPost.value.length===0) contentPost.setAttribute('placeholder','Commentaire Vide : saisissez votre avis pour publier')
        else formPostComment.submit()
     })

})



